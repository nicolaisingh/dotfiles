#
# /etc/bash.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

set -o vi

# For autocomplete in SBCL
export BREAK_CHARS="\"#'(),;\`\\|!?[]{}"
alias sbcl="rlwrap -b \$BREAK_CHARS sbcl"

# GNU screen + vim + lisp dev environment
alias scrimp='screen -c ~/.screenrc-scrimp -S scrimp'

# Aliases
alias ..='cd ..'
alias 1='ls -1'
alias 1a='ls -1a'
alias cgrep='grep --color=always '
alias chgrp='chgrp --preserve-root'
alias chmod='chmod --preserve-root'
alias chown='chown --preserve-root'
alias cp='cp -i'
alias df='df -h'
alias diff='colordiff '
alias du='du -h'
alias gimp='gimp --no-splash'
alias grep='grep --color=auto'
alias l='ls'
alias la='ls -a'
alias ll='ls -lh'
alias lla='ls -lah'
alias ln='ln -i'
alias ls='ls --color=auto'
alias mv='mv -i'
alias primusrun='vblank_mode=0 primusrun '
alias rm='rm -I --preserve-root'
alias rexr='xrdb -merge ~/.Xresources'
alias mpv='mpv --jack-port=jamin'

# Aliases for non-root only
if [[ $UID -ne 0 ]]; then
	alias powerpill='sudo powerpill'
	alias pacman='sudo pacman'
	alias sc='sudo systemctl'
	alias scu='systemctl --user'
	alias svim='sudo rvim'
fi

# Aliases for root only
if [[ $UID -eq 0 ]]; then
	alias vim='rvim'
fi

# Auto cd when path is entered
# shopt -s autocd

# Line wrap on window resize
shopt -s checkwinsize

#\[\e[41m\] \[\e[m\]
if [[ $UID -ne 0 ]]; then
	PS1='\[\e[0;47m\] \u \[\e[m\]\[\e[0;32m\] \W \[\e[m\]\[\e[0m\] '
else
	PS1='\[\e[0;41m\] \u \[\e[m\]\[\e[0;32m\] \W \[\e[m\]\[\e[0m\] '
fi
#PS1='[\u@\h \W]\$ '
PS2='> '
PS3='> '
PS4='+ '

case ${TERM} in
  xterm*|rxvt*|Eterm|aterm|kterm|gnome*)
    PROMPT_COMMAND=${PROMPT_COMMAND:+$PROMPT_COMMAND; }'printf "\033]0;%s@%s:%s\007" "${USER}" "${HOSTNAME%%.*}" "${PWD/#$HOME/\~}"'

    ;;
  screen)
    PROMPT_COMMAND=${PROMPT_COMMAND:+$PROMPT_COMMAND; }'printf "\033_%s@%s:%s\033\\" "${USER}" "${HOSTNAME%%.*}" "${PWD/#$HOME/\~}"'
    ;;
esac

[ -r /usr/share/bash-completion/bash_completion   ] && . /usr/share/bash-completion/bash_completion
