Nicolai Singh's Dotfiles
========================

A collection of dotfiles for my Linux setup.

The files included here are from all the customizations I applied
while exploring different options and configurations for whatever
software I encounter and use.
